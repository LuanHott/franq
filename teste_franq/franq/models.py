from django.db import models
from django.contrib.auth.models import AbstractUser

class Pessoa(AbstractUser):
    nome = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    telefone = models.CharField(max_length=9,unique=True)
    email = models.CharField(max_length=200,unique=True)
    admin = models.BooleanField(default=False)
    USERNAME_FIELD = 'telefone'
        
