from django.urls import include, path
from rest_framework import routers
from .api import PessoaViewSet
from . import views

api_router = routers.DefaultRouter()
api_router.register(r"pessoaapi", PessoaViewSet, basename="pessoa")

urlpatterns = [
    path('api/', include(api_router.urls)),
    path('login/', views.loginuser, name='login', ),
    path('scookie',views.setcookie, name='scookie'), 
    path('home/', views.home, name='home', ),
    path('registro/', views.registro, name='registro', ),
]