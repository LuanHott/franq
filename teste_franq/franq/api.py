from django.db.models import query
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .serializers import *

from .models import Pessoa
from .serializers import PessoaSerializer

class PessoaViewSet(viewsets.ModelViewSet):
    serializer_class = PessoaSerializer
    def get_queryset(self):        
        if self.request.query_params.get('email') is not None:
            email1 = self.request.query_params.get('email')
            queryset = Pessoa.objects.filter(email=email1)
        else:
            queryset = Pessoa.objects.all()
        return queryset

