from django.db.models.fields import EmailField
from django.shortcuts import render,redirect   
from django.http import HttpResponse
from .forms import PessoaForm, PessoaLoginForm
from .models import Pessoa
from django.core.cache import cache
from django.contrib import messages
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth import authenticate, login as auth_login
import requests

def home(request):
    return render(request,'home.html')

def registro(request):
    form = PessoaForm(request.POST or None)
    def clean(self):
        try:
            Pessoa.objects.get(nome = self.cleaned_data['nome'],
                                telefone = self.cleaned_data['telefone'],
                                email = self.cleaned_data['email'],
                                admin = self.cleaned_data['admin'],)
            raise PessoaForm.ValidationError("Email ou telefone já cadastrados")   
        except Pessoa.DoesNotExist:
            pass
        return self.cleaned_data    
    if request.method == 'POST' and form.is_valid():
        messages.add_message(request, messages.INFO, 'Pessoa cadastrada com sucesso.')
        
        email = form.cleaned_data.get('email')
        telefone = form.cleaned_data.get('telefone')
        admin = form.cleaned_data.get('admin')
        nome = form.cleaned_data.get('nome')

        form.save()
        url = 'http://127.0.0.1:8000/login/api/pessoaapi/'
        obj = {'telefone': telefone, 'email': email, 'admin': admin}
        
        urlg ='http://127.0.0.1:8000/login/api/garagemapi/'
        objg = {'nome': 'Garagem de {}'.format(nome),
                'dono': telefone}

        x = requests.post(url, data = obj)
        x1 = requests.post(urlg, data = objg)       

    return render(request, 'index.html', {'form': form})

def loginuser(request):
    context={}
    
    if request.method=="POST":

        form=PessoaLoginForm(data=request.POST)
        if form.is_valid():
            
            telefone = form.cleaned_data.get('telefone')
            password = form.cleaned_data.get('password')

            telefoneauth = form.cleaned_data.get('username')

            user = authenticate(request, telefone=telefone, password=password)

            request.session['authtel'] = telefoneauth           
            if user is not None:
                auth_login(request, user)             
            return redirect('scookie')
        

    else:
        form=PessoaLoginForm()


    context['form']=form
    return render(request,'login.html',context)

def setcookie(request):  
    
    telefone = request.session['authtel']
    print (telefone)
    if telefone is not None:
        response = HttpResponse("Cookie colocado")  
        response.set_cookie('telauth', telefone)  
        return response
