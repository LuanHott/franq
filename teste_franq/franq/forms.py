from django import forms
from .models import Pessoa
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth import login,authenticate,logout

class PessoaForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = Pessoa
        fields = UserCreationForm.Meta.fields + ('nome','telefone','email')

class PessoaLoginForm(AuthenticationForm):

    class Meta:
        model=Pessoa
        fields=("telefone","password")

        def clean(self):
            print("entrou no form")
            if self.is_valid():
                print("é valido no form")
                email1=self.cleaned_data['telefone']
                password1=self.cleaned_data['password']
                if not authenticate(telefone=email1,password=password1):
                    raise forms.ValidationError("Invalid LOGIN")
