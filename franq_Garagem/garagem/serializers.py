from rest_framework import serializers
from .models import Pessoa,Garagem

class PessoaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pessoa
        fields = '__all__'

class GaragemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Garagem
        fields = '__all__'