from django.db.models.fields import EmailField
from django.shortcuts import render,redirect 
from django.http import HttpResponse
from .forms import PessoaForm,CarroForm
from .models import Pessoa,Veiculo
from django.contrib import messages
import json
from django.db import connection



def login(request):
    form = PessoaForm(request.POST or None)
    def clean(self):

        try:
            Pessoa.objects.get( telefone = self.cleaned_data['telefone'],
                                email = self.cleaned_data['email'],)
            raise PessoaForm.ValidationError("Telefone: ",Pessoa.telefone,"Email: ", Pessoa.email)

        except Pessoa.DoesNotExist:
            messages.add_message(request, messages.INFO, 'Credenciais inválidas.')
        return self.cleaned_data  
          
    if request.method == 'POST' and form.is_valid():
        messages.add_message(request, messages.INFO, 'Form válido')

    return render(request, 'login.html', {'form': form})

def garagem(request):

    telefone = request.COOKIES.get('telauth')
    print (Veiculo.objects.filter(garagem=telefone))

    return render(request,'garagem.html', {'carros': Veiculo.objects.filter(garagem=telefone)})

def carros(request):
    form = CarroForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
        telefone = request.COOKIES.get('telauth')
        carros = form.cleaned_data.get('carros')
        veiculo = str(carros).split()
        if veiculo[0] == 'carro':           
            Veiculo.objects.filter(moto=False,ano_fabricacao=veiculo[2],cor=veiculo[1]).update(garagem=telefone)
        else:
            Veiculo.objects.filter(moto=True,ano_fabricacao=veiculo[2],modelo=veiculo[1]).update(garagem=telefone)


    return render(request, 'carros.html', {'form': form})

