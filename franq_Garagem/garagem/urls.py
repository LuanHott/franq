from django.urls import include, path

from rest_framework import routers
from .api import PessoaViewSet,GaragemViewSet
from . import views

api_router = routers.DefaultRouter()
api_router.register(r"pessoaapi", PessoaViewSet)
api_router.register(r"garagemapi", GaragemViewSet)

urlpatterns = [
    path("api/", include(api_router.urls)),
    path("carros", views.carros, name="carros"),
    path("garagem", views.garagem, name="garagem")
]