from django import forms
from django.db.models import fields
from django.db.models.fields import EmailField
from .models import Pessoa, Veiculo

class PessoaForm(forms.ModelForm):

    class Meta:
        model = Pessoa
        fields = ['telefone', 'email']

class CarroForm(forms.Form):
        carros = forms.ModelChoiceField(queryset=Veiculo.objects.filter(garagem__isnull=True).exclude(garagem__exact=''), empty_label=None)
        def cleancarros(self):
            carroselec = self.cleaned_data['carros']
            return carroselec
    