from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .serializers import *

from .models import Pessoa
from .serializers import PessoaSerializer

class PessoaViewSet(viewsets.ModelViewSet):
    queryset = Pessoa.objects.all()
    serializer_class = PessoaSerializer

class GaragemViewSet(viewsets.ModelViewSet):
    queryset = Garagem.objects.all()
    serializer_class = GaragemSerializer
