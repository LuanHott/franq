from django.db import models

class Garagem(models.Model):
    nome = models.CharField(max_length=200)
    dono = models.CharField(max_length=11)

class Veiculo(models.Model):
    garagem = models.CharField(max_length=200,null=True,blank=True)
    cor = models.CharField(max_length=200)
    ano_fabricacao = models.CharField(max_length=4)
    modelo = models.CharField(max_length=200)
    moto = models.BooleanField(default=False)
    def __str__(self):
        if self.moto == True:
            label = "moto " + self.modelo + " " + self.ano_fabricacao
            return label
        else:
            label = "carro " + self.cor + " " + self.ano_fabricacao
            return label


class Pessoa(models.Model):
    telefone = models.CharField(max_length=9)
    email = models.CharField(max_length=200)
    admin = models.BooleanField(default=False)
    class Meta:
        unique_together = ["telefone", "email"]
